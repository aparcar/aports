# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcmutils
pkgver=5.83.0
pkgrel=0
pkgdesc="Utilities for interacting with KCModules"
arch="all !armhf !mips64 !s390x" # armhf blocked by extra-cmake-modules and mips64, s390x blocked by polkit
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="kitemviews-dev kconfigwidgets-dev kcoreaddons-dev ki18n-dev kiconthemes-dev kservice-dev kxmlgui-dev kdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcmutils-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="
fe06fc60adf2df8a39fd69409abe9db6db62dab3ef09c81259285222d79710ad393c1bacf40cb303a37bbb7d80025f16f630e23897f32d3322d4d6e80342bf65  kcmutils-5.83.0.tar.xz
"
