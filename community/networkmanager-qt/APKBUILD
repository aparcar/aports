# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=networkmanager-qt
pkgver=5.83.0
pkgrel=0
pkgdesc="Qt wrapper for NetworkManager API"
arch="all !armhf" # armhf blocked by extra-cmake-modules
arch="$arch !mips !mips64 !s390x" # polkit
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="networkmanager"
depends_dev="networkmanager-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/networkmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# The excluded tests currently fail
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(manager|settings|activeconnection)test'
}


package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="
d7d0aea9c1b13ace7d651253453ce8996d26f3acc8145235f675f2b8c1f4bd21206eda03fc2975773f459cd72d8d0e1539168be79949d0a11c4b4bde64357b70  networkmanager-qt-5.83.0.tar.xz
"
